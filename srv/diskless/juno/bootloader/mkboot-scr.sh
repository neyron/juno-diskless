#!/bin/bash
SCRIPT=$1
if [ -z "$SCRIPT" -a "${SCRIPT##*.}" != "script" ]; then
  echo "give a boot script" 1>&2
  exit 1
fi
mkimage -A arm64 -O linux -T script -C gzip -a 0x90000000 -e 0x90000000 -d $SCRIPT -n "arm64-nfsroot" ${SCRIPT%.script}.scr
