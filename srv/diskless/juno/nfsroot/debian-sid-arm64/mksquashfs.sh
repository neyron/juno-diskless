#!/bin/bash
SQSH_FILE=${1:-rootfs.sqsh}
echo "Creating squashfs file: $SQSH_FILE ..."
mksquashfs . $SQSH_FILE -comp xz -noappend -wildcards -e overlay -e rootfs.sqsh -e sqsh-to-tmpfs -e no-overlay -e $SQSH_FILE -e run/* -e home/* -e tmp/* -e var/tmp/* -e sys/* -e proc/* -e dev/* -e var/cache/apt/archives/*.deb
